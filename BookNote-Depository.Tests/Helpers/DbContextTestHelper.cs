using System;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.Tests.Helpers
{
    public static class DbContextTestHelper
    {
        /// <summary>
        /// Get DbContextOptions using an in-memory database for instansiating a DbContext
        /// </summary>
        public static DbContextOptions GetOptions<TContext>() where TContext : DbContext
             => new DbContextOptionsBuilder<TContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
    }
}