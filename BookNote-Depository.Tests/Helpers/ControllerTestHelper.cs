using System;
using AutoMapper;
using BookNote_Depository.App.AutoMapper;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace BookNote_Depository.Tests.Helpers
{
    public class ControllerTestHelper
    {
        public static TController GetController<TController, TEntity>(ApplicationDbContext context, string baseUri)
            where TController : Microsoft.AspNetCore.Mvc.Controller
            where TEntity : class
        {
            IRepository<TEntity> repository = new Repository<TEntity>(context);
            MapperConfiguration mapperConfig = new MapperConfiguration(c => c.AddProfile<DomainProfile>());

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            ILinkHeaderWriter<TEntity> headerWriter = new LinkHeaderWriter<TEntity>(uriService, context);

            TController controller = (TController)Activator.CreateInstance(typeof(TController), repository, mapperConfig.CreateMapper(), headerWriter);

            ControllerContext controllerContext = GetControllerContext();
            controller.ControllerContext = controllerContext;

            return controller;
        }

        public static ControllerContext GetControllerContext()
        {
            Mock<HttpRequest> mockRequest = new Mock<HttpRequest>();
            mockRequest.Setup(x => x.Path).Returns(PathString.FromUriComponent("/api"));

            Mock<HttpResponse> mockResponse = new Mock<HttpResponse>();
            mockResponse.Setup(m => m.Headers).Returns(new HeaderDictionary());

            HttpContext httpContext = Mock.Of<HttpContext>(_ => 
                _.Request == mockRequest.Object &&
                _.Response == mockResponse.Object);

            ControllerContext controllerContext = new ControllerContext() 
                { HttpContext = httpContext };

            return controllerContext;
        }
    }
}