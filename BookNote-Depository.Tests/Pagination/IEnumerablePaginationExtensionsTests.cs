using System.IO;
using System;
using System.Collections.Generic;
using BookNote_Depository.App.Models.Domain;
using Xunit;
using BookNote_Depository.App.Core.Extensions.Pagination;
using BookNote_Depository.App.Models.Pagination;
using System.Linq;

namespace BookNote_Depository.Tests.Pagination
{
    public class IEnumerablePaginationExtensionsTests
    {
        #region IEnumerable
        [Fact]
        public void Paginate_IEnumerable_ReturnsTheSetAmount()
        {
            //Assign
            IEnumerable<Author> authors = new List<Author> { new Author(), new Author() };

            PaginationFilter filter = new PaginationFilter { Page = 1, Size = 1 };

            //Act
            IEnumerable<Author> paginatedAuthors = authors.Paginate(filter);

            //Assert
            Assert.Equal(filter.Size, paginatedAuthors.Count());
        }

        [Fact]
        public void Paginate_IEnumerable_ReturnsNull_IfFilterPageIsZero()
        {
            //Assign
            IEnumerable<Author> authors = new List<Author>();

            PaginationFilter filter = new PaginationFilter { Page = 0, Size = 1 };

            //Act
            IEnumerable<Author> paginatedAuthors = authors.Paginate(filter);

            //Assert
            Assert.Null(paginatedAuthors);
        }

        [Fact]
        public void Paginate_IEnumerable_ReturnsNull_IfFilterSizeIsZero()
        {
            //Assign
            IEnumerable<Author> authors = new List<Author>();

            PaginationFilter filter = new PaginationFilter { Page = 1, Size = 0 };

            //Act
            IEnumerable<Author> paginatedAuthors = authors.Paginate(filter);

            //Assert
            Assert.Null(paginatedAuthors);
        }
        #endregion

        #region IQueryable
        [Fact]
        public void Paginate_IQueryable_ReturnsTheSetAmount()
        {
            //Assign
            IQueryable<Author> authors = new List<Author> { new Author(), new Author() }.AsQueryable();

            PaginationFilter filter = new PaginationFilter { Page = 1, Size = 1 };

            //Act
            IQueryable<Author> paginatedAuthors = authors.Paginate(filter);

            //Assert
            Assert.Equal(filter.Size, paginatedAuthors.Count());
        }

        [Fact]
        public void Paginate_IQueryable_ReturnsNull_IfFilterPageIsZero()
        {
            //Assign
            IQueryable<Author> authors = new List<Author>().AsQueryable();

            PaginationFilter filter = new PaginationFilter { Page = 0, Size = 1 };

            //Act
            IQueryable<Author> paginatedAuthors = authors.Paginate(filter);

            //Assert
            Assert.Null(paginatedAuthors);
        }

        [Fact]
        public void Paginate_IQueryable_ReturnsNull_IfFilterSizeIsZero()
        {
            //Assign
            IQueryable<Author> authors = new List<Author>().AsQueryable();

            PaginationFilter filter = new PaginationFilter { Page = 1, Size = 0 };

            //Act
            IQueryable<Author> paginatedAuthors = authors.Paginate(filter);

            //Assert
            Assert.Null(paginatedAuthors);
        }
        #endregion
    }
}