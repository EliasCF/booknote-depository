using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.AspNetCore.Http;
using Xunit;
using System.Linq;

namespace BookNote_Depository.Tests.Pagination
{
    public class LinkheaderWriterTests
    {
        string baseUri = "https://localhost:5001";

        [Theory]
        [InlineData(3, 1, 2)]
        public void Write_WithoutQuery_PageNumberBiggerThanTotalPageAmount_ReturnsNoHeaders(int page, int size, int entities)
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.AddRange(Enumerable.Range(1, entities).Select(x => new Author()));
            context.SaveChanges();

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);

            PaginationFilter filter = new PaginationFilter { Page = page, Size = size };
            
            //Act
            headerWriter.Write(headerDictionary, filter, "/api/authors");
            string[] linkHeader = headerDictionary.GetCommaSeparatedValues("Link");
            
            //Assert
            Assert.Empty(headerDictionary);
        }

        [Theory]
        [InlineData(1, 1, 2)]
        [InlineData(1, 10, 20)]
        public void Write_WithoutQuery_FirstPageOutOfTwo_GivesSubstrings_First_Next_Last(int page, int size, int entities)
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.AddRange(Enumerable.Range(1, entities).Select(x => new Author()));
            context.SaveChanges();

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);

            PaginationFilter filter = new PaginationFilter { Page = page, Size = size };
            
            //Act
            headerWriter.Write(headerDictionary, filter, "/api/authors");
            string[] linkHeader = headerDictionary.GetCommaSeparatedValues("Link");
            
            //Assert
            Assert.Single(headerDictionary);
            Assert.Contains("rel=\"first\"", linkHeader.ElementAt(0));
            Assert.Contains("rel=\"next\"", linkHeader.ElementAt(1));
            Assert.Contains("rel=\"last\"", linkHeader.ElementAt(2));
            Assert.Equal(3, linkHeader.Count());
        }

        [Theory]
        [InlineData(2, 1, 2)]
        [InlineData(2, 10, 20)]
        public void Write_WithoutQuery_SecondPageOutOfTwo_GivesSubstrings_First_Prev_Last(int page, int size, int entities)
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.AddRange(Enumerable.Range(1, entities).Select(x => new Author()));
            context.SaveChanges();

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);

            PaginationFilter filter = new PaginationFilter { Page = page, Size = size };
            
            //Act
            headerWriter.Write(headerDictionary, filter, "/api/authors");
            string[] linkHeader = headerDictionary.GetCommaSeparatedValues("Link");
            
            //Assert
            Assert.Single(headerDictionary);
            Assert.Contains("rel=\"first\"", linkHeader.ElementAt(0));
            Assert.Contains("rel=\"prev\"", linkHeader.ElementAt(1));
            Assert.Contains("rel=\"last\"", linkHeader.ElementAt(2));
            Assert.Equal(3, linkHeader.Count());
        }

        [Theory]
        [InlineData(1, 1, 3)]
        [InlineData(1, 10, 30)]
        public void Write_WithoutQuery_FirstPageOutOfThree_GivesSubstrings_First_Next_Last(int page, int size, int entities)
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.AddRange(Enumerable.Range(1, entities).Select(x => new Author()));
            context.SaveChanges();

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);

            PaginationFilter filter = new PaginationFilter { Page = page, Size = size };
            
            //Act
            headerWriter.Write(headerDictionary, filter, "/api/authors");
            string[] linkHeader = headerDictionary.GetCommaSeparatedValues("Link");
            
            //Assert
            Assert.Single(headerDictionary);
            Assert.Contains("rel=\"first\"", linkHeader.ElementAt(0));
            Assert.Contains("rel=\"next\"", linkHeader.ElementAt(1));
            Assert.Contains("rel=\"last\"", linkHeader.ElementAt(2));
            Assert.Equal(3, linkHeader.Count());
        }

        [Theory]
        [InlineData(2, 1, 3)]
        [InlineData(2, 10, 30)]
        public void Write_WithoutQuery_SecondPageOutOfThree_GivesSubstrings_First_Prev_Next_Last(int page, int size, int entities)
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.AddRange(Enumerable.Range(1, entities).Select(x => new Author()));
            context.SaveChanges();

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);

            PaginationFilter filter = new PaginationFilter { Page = page, Size = size };
            
            //Act
            headerWriter.Write(headerDictionary, filter, "/api/authors");
            string[] linkHeader = headerDictionary.GetCommaSeparatedValues("Link");
            
            //Assert
            Assert.Single(headerDictionary);
            Assert.Contains("rel=\"first\"", linkHeader.ElementAt(0));
            Assert.Contains("rel=\"prev\"", linkHeader.ElementAt(1));
            Assert.Contains("rel=\"next\"", linkHeader.ElementAt(2));
            Assert.Contains("rel=\"last\"", linkHeader.ElementAt(3));
            Assert.Equal(4, linkHeader.Count());
        }

        [Theory]
        [InlineData(3, 1, 3)]
        [InlineData(3, 10, 30)]
        public void Write_WithoutQuery_ThirdPageOutOfThree_GivesSubstrings_First_Prev_Last(int page, int size, int entities)
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.AddRange(Enumerable.Range(1, entities).Select(x => new Author()));
            context.SaveChanges();

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);

            PaginationFilter filter = new PaginationFilter { Page = page, Size = size };
            
            //Act
            headerWriter.Write(headerDictionary, filter, "/api/authors");
            string[] linkHeader = headerDictionary.GetCommaSeparatedValues("Link");
            
            //Assert
            Assert.Single(headerDictionary);
            Assert.Contains("rel=\"first\"", linkHeader.ElementAt(0));
            Assert.Contains("rel=\"prev\"", linkHeader.ElementAt(1));
            Assert.Contains("rel=\"last\"", linkHeader.ElementAt(2));
            Assert.Equal(3, linkHeader.Count());
        }

        [Fact]
        public void Write_WithoutQuery_IsEmpty_IfNoEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);
            
            //Act
            headerWriter.Write(headerDictionary, new PaginationFilter(), "/api/authors");
            
            //Assert
            Assert.Empty(headerDictionary);
        }

        [Fact]
        public void Write_WithQuery_IsEmpty_IfNoEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            HeaderDictionary headerDictionary = new HeaderDictionary();
            ILinkHeaderWriter<Author> headerWriter = new LinkHeaderWriter<Author>(uriService, context);
            
            //Act
            headerWriter.Write<Book>(headerDictionary, new PaginationFilter(), "/api/authors", x => x
                .SelectMany(a => a.BookAuthors)
                .Select(b => b.Book));
            
            //Assert
            Assert.Empty(headerDictionary);
        }
    }
}