using System;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Models.Pagination;
using Xunit;

namespace BookNote_Depository.Tests.Pagination
{
    public class PaginationUriServiceTests
    {
        [Fact]
        public void GetPageUri_ReturnsCorrectUri()
        {
            //Assign
            string baseUri = "https://localhost:5001";
            string route = "/api/books";

            PaginationFilter filter = new PaginationFilter();
            Uri expectedUri = new Uri($"{baseUri}{route}?Page={filter.Page}&Size={filter.Size}");

            IPaginationUriService uriService = new PaginationUriService(baseUri);

            //Act
            Uri uri = uriService.GetPageUri(filter, route);

            //Assert
            Assert.Equal(expectedUri.ToString(), uri.ToString());
        }

        [Fact]
        public void GetPageUri_ReturnsNull_IfRouteIsNullOrEmpty()
        {
            //Assign
            string baseUri = "https://localhost:5001";

            PaginationFilter filter = new PaginationFilter();

            IPaginationUriService uriService = new PaginationUriService(baseUri);

            //Act
            Uri firstUri = uriService.GetPageUri(filter, null);
            Uri secondUri = uriService.GetPageUri(filter, string.Empty);

            //Assert
            Assert.Null(firstUri);
            Assert.Null(secondUri);
        }

        [Fact]
        public void GetPageUri_ReturnsNull_IfPaginationFilterIsNull()
        {
            //Assign
            string baseUri = "https://localhost:5001";
            string route = "/api/books";

            PaginationFilter filter = new PaginationFilter();
            Uri expectedUri = new Uri($"{baseUri}{route}?Page={filter.Page}&Size={filter.Size}");

            IPaginationUriService uriService = new PaginationUriService(baseUri);

            //Act
            Uri uri = uriService.GetPageUri(null, route);

            //Assert
            Assert.Null(uri);
        }

        [Fact]
        public void GetPageUri_ReturnsNull_IfConstructorStringIsNullOrEmpty()
        {
            //Assign
            string route = "/api/books";

            PaginationFilter filter = new PaginationFilter();

            IPaginationUriService firstUriService = new PaginationUriService(null);
            IPaginationUriService secondUriService = new PaginationUriService(string.Empty);

            //Act
            Uri firstUri = firstUriService.GetPageUri(filter, route);
            Uri secondUri = secondUriService.GetPageUri(filter, route);

            //Assert
            Assert.Null(firstUri);
            Assert.Null(secondUri);
        }
    }
}