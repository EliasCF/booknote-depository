using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Extensions.Repository;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;

namespace BookNote_Depository.Tests.Repository
{
    public class BookRepositoryExtensionsTests
    {
        #region GetAuthorsByBookId
        [Fact]
        public async Task GetAuthorsByBookId_ReturnsAuthors_IfBookExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());

            context.BookAuthors.Add(new BookAuthor
            {
                BookId = bookEntry.Entity.Id,
                AuthorId = authorEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Book> repository = new Repository<Book>(context);

            //Act
            List<Author> authors = await repository.GetAuthorsByBookId(bookEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(authors);
        }

        [Fact]
        public async Task GetAuthorsByBookId_ReturnsNull_IfBookDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<Book> repository = new Repository<Book>(context);

            //Act
            List<Author> authors = await repository.GetAuthorsByBookId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Null(authors);
        }

        [Fact]
        public async Task GetAuthorsByBookId_ReturnsNull_IfPaginationFilterIsInvalid()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());

            context.BookAuthors.Add(new BookAuthor
            {
                BookId = bookEntry.Entity.Id,
                AuthorId = authorEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Book> repository = new Repository<Book>(context);

            //Act
            List<Author> authors = await repository.GetAuthorsByBookId(bookEntry.Entity.Id, new PaginationFilter { Page = 0, Size = 0 }, new CancellationTokenSource().Token);

            //Assert
            Assert.Null(authors);
        }
        #endregion

        #region GetUsersByBookId
        [Fact]
        public async Task GetUsersByBookId_ReturnsUsers_IfBookExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<User> userEntry = context.Users.Add(new User());

            context.UserBooks.Add(new UserBook
            {
                BookId = bookEntry.Entity.Id,
                UserId = userEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Book> repository = new Repository<Book>(context);

            //Act
            List<User> users = await repository.GetUsersByBookId(bookEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(users);
        }

        [Fact]
        public async Task GetUsersByBookId_ReturnsNull_IfBookDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<Book> repository = new Repository<Book>(context);

            //Act
            List<User> users = await repository.GetUsersByBookId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Null(users);
        }

        [Fact]
        public async Task GetUsersByBookId_ReturnsNull_IfPaginationFilterIsInvalid()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<User> userEntry = context.Users.Add(new User());

            context.UserBooks.Add(new UserBook
            {
                BookId = bookEntry.Entity.Id,
                UserId = userEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Book> repository = new Repository<Book>(context);

            //Act
            List<User> users = await repository.GetUsersByBookId(bookEntry.Entity.Id, new PaginationFilter { Page = 0, Size = 0 }, new CancellationTokenSource().Token);

            //Assert
            Assert.Null(users);
        }
        #endregion
    }
}