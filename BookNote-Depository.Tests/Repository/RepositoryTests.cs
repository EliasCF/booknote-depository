using System;
using System.Collections.Generic;
using System.Linq;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Models.Domain;
using Xunit;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using BookNote_Depository.App.Data;
using BookNote_Depository.Tests.Helpers;
using BookNote_Depository.App.Models.Pagination;

namespace BookNote_Depository.Tests
{
    public class RepositoryTests
    {
        #region AsQueryable
        [Fact]
        public void AsQueryable_Returns()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            IQueryable<Author> books = repository.AsQueryable();

            //Assert
            Assert.Single(books);
        }
        #endregion

        #region DeleteAsync
        [Fact]
        public void DeleteAsync_InvalidArgument_ThrowsException()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<Author> repository = new Repository<Author>(context);

            //Act & Assert
            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await repository.DeleteAsync(Guid.Empty);
            });
        }

        [Fact]
        public async Task DeleteAsync_ReturnsFalse_IfEntity_DoesnNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool deletionResult = await repository.DeleteAsync(Guid.NewGuid());

            //Assert
            Assert.False(deletionResult);
        }

        [Fact]
        public async Task DeleteAsync_ReturnsTrue_IfEntity_Exists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> entry = context.Authors.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool deletionResult = await repository.DeleteAsync(entry.Entity.Id);

            //Assert
            Assert.True(deletionResult);
        }
        #endregion

        #region GetAsync
        [Fact]
        public async Task GetAsync_Returns()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            List<Author> authors = await repository.GetAsync(new PaginationFilter());

            //Assert
            Assert.Single(authors);
        }

        [Fact]
        public async Task GetAsync_ReturnsNull_IfPaginationFilterIsInvalid()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            List<Author> authors = await repository.GetAsync(new PaginationFilter { Page = 0, Size = 0 });

            //Assert
            Assert.Null(authors);
        }

        [Fact]
        public async Task GetAsync_ReturnsAuthor_IfEntity_Exists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> entry = context.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            Author author = await repository.GetAsync(entry.Entity.Id);

            //Assert
            Assert.Equal(entry.Entity.Id, author.Id);
        }

        [Fact]
        public async Task GetAsync_ReturnsNull_IfEntity_DoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> entry = context.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);
            Guid nonExistentId = Guid.NewGuid();

            //Act
            Author author = await repository.GetAsync(nonExistentId);

            //Assert
            Assert.Null(author);
        }

        [Fact]
        public void GetAsync_InvalidArgument_ThrowsException()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<Author> repository = new Repository<Author>(context);

            //Act & Assert
            Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await repository.GetAsync(Guid.Empty);
            });
        }
        #endregion

        #region InsertAsync
        [Fact]
        public void InsertAsync_InvalidArgument_ThrowsException()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<Author> repository = new Repository<Author>(context);

            //Act & Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                await repository.InsertAsync(null);
            });
        }

        [Fact]
        public async Task InsertAsync_InsertsAndReturns_Entity()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<Author> repository = new Repository<Author>(context);
            Author authorToInsert = new Author { Name = "John Doe" };

            //Act
            Author author = await repository.InsertAsync(authorToInsert);

            //Assert
            Assert.Equal(authorToInsert.Name, author.Name);
        }
        #endregion

        #region UpdateAsync
        [Fact]
        public void UpdateAsync_InvalidArgument_ThrowsException()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<Author> repository = new Repository<Author>(context);

            //Act & Assert
            Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                await repository.InsertAsync(null);
            });
        }

        [Fact]
        public async Task UpdateAsync_UpdatesAndReturns_Entity()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            Author author = new Author { Name = "John Doe" };

            Author authorToUpdate = context.Add(author).Entity;
            context.SaveChanges();
            authorToUpdate.Name = "Jane Doe";

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            Author updatedAuthor = await repository.UpdateAsync(authorToUpdate);

            //Assert
            Assert.Equal(authorToUpdate.Name, updatedAuthor.Name);
        }
        #endregion
    }
}