using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Extensions.Repository;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;

namespace BookNote_Depository.Tests.Repository
{
    public class UserRepositoryExtensionsTests
    {
        #region GetNotesByUserId
        [Fact]
        public async Task GetNotesByUserId_ReturnsNotes_IfUserExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntry = context.Books.Add(new Book());

            EntityEntry<UserBook> userBookEntry = context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id,
            });

            context.Notes.Add(new Note
            {
                UserBookBookId = bookEntry.Entity.Id,
                UserBookUserId = userEntry.Entity.Id,
                UserBook = userBookEntry.Entity
            });

            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            List<Note> notes = await repository.GetNotesByUserId(userEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(notes);
        }

        [Fact]
        public async Task GetNotesByUserId_ReturnsNull_IfUserDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<User> repository = new Repository<User>(context);

            //Act
            List<Note> notes = await repository.GetNotesByUserId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Null(notes);
        }

        [Fact]
        public async Task GetNotesByUserId_ReturnsNull_IfPaginationFilterIsInvalid()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntry = context.Books.Add(new Book());

            EntityEntry<UserBook> userBookEntry = context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id,
            });

            context.Notes.Add(new Note
            {
                UserBookBookId = bookEntry.Entity.Id,
                UserBookUserId = userEntry.Entity.Id,
                UserBook = userBookEntry.Entity
            });

            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            List<Note> notes = await repository.GetNotesByUserId(userEntry.Entity.Id, new PaginationFilter { Page = 0, Size = 0}, new CancellationTokenSource().Token);

            //Assert
            Assert.Null(notes);
        }
        #endregion

        #region GetBooksByUserId
        [Fact]
        public async Task GetBooksByUserId_ReturnsBooks_IfUserExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntry = context.Books.Add(new Book());

            EntityEntry<UserBook> userBookEntry = context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id,
            });

            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            List<Book> books = await repository.GetBooksByUserId(userEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(books);
        }

        [Fact]
        public async Task GetBooksByUserId_ReturnsNull_IfUserDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<User> repository = new Repository<User>(context);

            //Act
            List<Book> books = await repository.GetBooksByUserId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Null(books);
        }

        [Fact]
        public async Task GetBooksByUserId_ReturnsNull_IfPaginationFilterIsInvalid()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntry = context.Books.Add(new Book());

            EntityEntry<UserBook> userBookEntry = context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id,
            });

            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            List<Book> books = await repository.GetBooksByUserId(userEntry.Entity.Id, new PaginationFilter { Page = 0, Size = 0 }, new CancellationTokenSource().Token);

            //Assert
            Assert.Null(books);
        }
        #endregion

        #region AddUserBook
        [Fact]
        public async Task AddUserBook_CreatesRelation_IfEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<User> userEntry = context.Users.Add(new User());
            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            bool result = await repository.AddUserBook(userEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.True(result);
            Assert.Single(bookEntry.Entity.Users);
            Assert.Single(userEntry.Entity.Books);
        }

        [Fact]
        public async Task AddUserBook_ReturnsFalse_IfEntities_DoNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<User> repository = new Repository<User>(context);

            //Act
            bool result = await repository.AddUserBook(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.False(result);
        }
        #endregion

        #region DeleteUserBook
        [Fact]
        public async Task DeleteUserBook_ReturnsTrue_IfEntityExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<User> userEntry = context.Users.Add(new User());

            context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            bool result = await repository.DeleteUserBook(userEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.True(result);
            Assert.Empty(bookEntry.Entity.Users);
            Assert.Empty(userEntry.Entity.Books);
        }

        [Fact]
        public async Task DeleteUserBook_ReturnsFalse_IfEntityDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<User> repository = new Repository<User>(context);

            //Act
            bool result = await repository.DeleteUserBook(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public async Task DeleteUserBook_ReturnsFalse_IfRelationDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<User> userEntry = context.Users.Add(new User());
            context.SaveChanges();

            IRepository<User> repository = new Repository<User>(context);

            //Act
            bool result = await repository.DeleteUserBook(userEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.False(result);
        }
        #endregion
    }
}