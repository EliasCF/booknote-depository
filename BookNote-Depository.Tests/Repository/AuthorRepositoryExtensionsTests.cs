using System;
using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Extensions.Repository;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;
using BookNote_Depository.Tests.Helpers;
using BookNote_Depository.App.Models.Pagination;

namespace BookNote_Depository.Tests.Repository
{
    public class AuthorRepositoryExtensionsTests
    {

        #region GetBooksByAuthorId
        [Fact]
        public async Task GetBooksByAuthorId_ReturnsEntity_IfItExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());

            context.BookAuthors.Add(new BookAuthor
            {
                BookId = bookEntry.Entity.Id,
                AuthorId = authorEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            List<Book> books = await repository.GetBooksByAuthorId(authorEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(books);
        }

        [Fact]
        public async Task GetBooksByAuthorId_ReturnsNull_IfEntityDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            List<Book> books = await repository.GetBooksByAuthorId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Null(books);
        }

        [Fact]
        public async Task GetBooksByAuthorId_ReturnsNull_IfPaginationFilterIsInvalid()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());


            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());

            context.BookAuthors.Add(new BookAuthor
            {
                BookId = bookEntry.Entity.Id,
                AuthorId = authorEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            List<Book> books = await repository.GetBooksByAuthorId(Guid.NewGuid(), new PaginationFilter { Page = 0, Size = 0 }, new CancellationTokenSource().Token);

            //Assert
            Assert.Null(books);
        }
        #endregion

        #region AddBookAuthor
        [Fact]
        public async Task AddBookAuthor_CreatesRelation_IfEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool result = await repository.AddBookAuthor(authorEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.True(result);
            Assert.Single(bookEntry.Entity.BookAuthors);
            Assert.Single(authorEntry.Entity.BookAuthors);
        }

        [Fact]
        public async Task AddBookAuthor_ReturnsFalse_IfEntities_DoNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool result = await repository.AddBookAuthor(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.False(result);
        }
        #endregion

        #region  DeleteBookAuthor
        [Fact]
        public async Task DeleteBookAuthor_ReturnsTrue_IfEntityExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());

            context.BookAuthors.Add(new BookAuthor
            {
                AuthorId = authorEntry.Entity.Id,
                BookId = bookEntry.Entity.Id
            });
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool result = await repository.DeleteBookAuthor(authorEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.True(result);
            Assert.Empty(bookEntry.Entity.BookAuthors);
            Assert.Empty(authorEntry.Entity.BookAuthors);
        }

        [Fact]
        public async Task DeleteBookAuthor_ReturnsFalse_IfEntityDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());
            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool result = await repository.DeleteBookAuthor(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public async Task DeleteBookAuthor_ReturnsFalse_IfRelationDoesNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());
            context.SaveChanges();

            IRepository<Author> repository = new Repository<Author>(context);

            //Act
            bool result = await repository.DeleteBookAuthor(authorEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.False(result);
        }
        #endregion
    }
}