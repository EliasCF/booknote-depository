using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Controllers;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;

namespace BookNote_Depository.Tests.Controller
{
    public class BookControllerTests
    {
        string baseUri = "https://localhost:5001";

        #region GetUsersByBookId
        [Fact]
        public async Task GetUsersByBookId_ReturnsUsers_IfBookExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntity = context.Books.Add(new Book());

            context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntity.Entity.Id
            });
            context.SaveChanges();

            BookController controller = ControllerTestHelper.GetController<BookController, Book>(context, baseUri);

            //Act
            ActionResult<List<UserDTO>> users = await controller.GetUsersByBookId(bookEntity.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(users.Value);
        }

        [Fact]
        public async Task GetUsersByBookId_ReturnsNotFound_IfBookDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            BookController controller = ControllerTestHelper.GetController<BookController, Book>(context, baseUri);

            //Act
            ActionResult<List<UserDTO>> users = await controller.GetUsersByBookId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (users.Result as StatusCodeResult).StatusCode);
        }
        #endregion

        #region GetAuthorByBookId
        [Fact]
        public async Task GetAuthorByBookId_ReturnsAuthors_IfBookExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());
            EntityEntry<Book> bookEntity = context.Books.Add(new Book());

            context.BookAuthors.Add(new BookAuthor
            {
                AuthorId = authorEntry.Entity.Id,
                BookId = bookEntity.Entity.Id
            });
            context.SaveChanges();

            BookController controller = ControllerTestHelper.GetController<BookController, Book>(context, baseUri);

            //Act
            ActionResult<List<AuthorDTO>> authors = await controller.GetAuthorByBookId(bookEntity.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(authors.Value);
        }

        [Fact]
        public async Task GetAuthorByBookId_ReturnsNotFound_IfBookDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            BookController controller = ControllerTestHelper.GetController<BookController, Book>(context, baseUri);

            //Act
            ActionResult<List<AuthorDTO>> authors = await controller.GetAuthorByBookId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (authors.Result as StatusCodeResult).StatusCode);
        }
        #endregion
    }
}