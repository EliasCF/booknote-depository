using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Controllers;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;

namespace BookNote_Depository.Tests.Controller
{
    public class UserControllerTests
    {
        string baseUri = "https://localhost:5001";

        #region GetNotesByUserId
        [Fact]
        public async Task GetNotesByUserId_ReturnsNotes_IfUserExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntry = context.Books.Add(new Book());

            EntityEntry<UserBook> userBookEntry = context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id,
            });

            context.Notes.Add(new Note
            {
                UserBookBookId = bookEntry.Entity.Id,
                UserBookUserId = userEntry.Entity.Id,
                UserBook = userBookEntry.Entity
            });
            context.SaveChanges();

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult<List<NoteDTO>> notes = await controller.GetNotesByUserId(userEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(notes.Value);
        }

        [Fact]
        public async Task GetNotesByUserId_ReturnsNotFound_IfUserDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult<List<NoteDTO>> notes = await controller.GetNotesByUserId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (notes.Result as StatusCodeResult).StatusCode);
        }
        #endregion

        #region GetBooksByUserId
        [Fact]
        public async Task GetBooksByUserId_ReturnsBooks_IfUserExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntry = context.Books.Add(new Book());

            context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntry.Entity.Id,
            });

            context.SaveChanges();

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult<List<BookDTO>> books = await controller.GetBooksByUserId(userEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(books.Value);
        }

        [Fact]
        public async Task GetBooksByUserId_ReturnsNotFound_IfUserDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult<List<BookDTO>> books = await controller.GetBooksByUserId(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (books.Result as StatusCodeResult).StatusCode);
        }
        #endregion

        #region AddUserBook
        [Fact]
        public async Task AddUserBook_ReturnsOK_IfEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Book> bookEntry = context.Books.Add(new Book());
            EntityEntry<User> userEntry = context.Users.Add(new User());
            context.SaveChanges();

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult result = await controller.AddUserBook(userEntry.Entity.Id, bookEntry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.OK, (result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public async Task AddUserBook_ReturnsNotFound_IfEntitiesDoNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult result = await controller.AddUserBook(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (result as StatusCodeResult).StatusCode);
        }
        #endregion

        #region DeleteUserBook
        [Fact]
        public async Task DeleteUserBook_ReturnsNoContent_IfEntitiesExit()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<User> userEntry = context.Users.Add(new User());
            EntityEntry<Book> bookEntity = context.Books.Add(new Book());

            context.UserBooks.Add(new UserBook
            {
                UserId = userEntry.Entity.Id,
                BookId = bookEntity.Entity.Id
            });
            context.SaveChanges();

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult result = await controller.DeleteUserBook(userEntry.Entity.Id, bookEntity.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NoContent, (result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public async Task DeleteUserBook_ReturnsNotFound_IfEntitiesDoNotExit()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            UserController controller = ControllerTestHelper.GetController<UserController, User>(context, baseUri);

            //Act
            ActionResult result = await controller.DeleteUserBook(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (result as StatusCodeResult).StatusCode);
        }
        #endregion
    }
}