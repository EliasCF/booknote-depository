using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Controllers;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;

namespace BookNote_Depository.Tests.Controller
{
    public class AuthorControllerTests
    {
        string baseUri = "https://localhost:5001";

        #region GetBooksByAuthor
        [Fact]
        public async Task GetBooksByAuthor_ReturnsBook_IfAuthorExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());
            EntityEntry<Book> bookEntity = context.Books.Add(new Book());

            context.BookAuthors.Add(new BookAuthor
            {
                AuthorId = authorEntry.Entity.Id,
                BookId = bookEntity.Entity.Id
            });
            context.SaveChanges();

            AuthorController controller = ControllerTestHelper.GetController<AuthorController, Author>(context, baseUri);

            //Act
            ActionResult<List<BookDTO>> books = await controller.GetBooksByAuthor(authorEntry.Entity.Id, new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(books.Value);
        }

        [Fact]
        public async Task GetBooksByAuthor_ReturnsNotFound_IfAuthorDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            AuthorController controller = ControllerTestHelper.GetController<AuthorController, Author>(context, baseUri);

            //Act
            ActionResult<List<BookDTO>> books = await controller.GetBooksByAuthor(Guid.NewGuid(), new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (books.Result as StatusCodeResult).StatusCode);
        }
        #endregion

        #region AddBookAuthor
        [Fact]
        public async Task AddBookAuthor_ReturnsOk_IfEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());
            EntityEntry<Book> bookEntity = context.Books.Add(new Book());
            context.SaveChanges();

            AuthorController controller = ControllerTestHelper.GetController<AuthorController, Author>(context, baseUri);

            //Act
            ActionResult result = await controller.AddBookAuthor(authorEntry.Entity.Id, bookEntity.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.OK, (result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public async Task AddBookAuthor_ReturnsNotFound_IfEntitiesDoNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            AuthorController controller = ControllerTestHelper.GetController<AuthorController, Author>(context, baseUri);

            //Act
            ActionResult result = await controller.AddBookAuthor(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (result as StatusCodeResult).StatusCode);
        }
        #endregion

        #region DeleteBookAuthor
        [Fact]
        public async Task DeleteBookAuthor_ReturnsOk_IfEntitiesExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> authorEntry = context.Authors.Add(new Author());
            EntityEntry<Book> bookEntity = context.Books.Add(new Book());

            context.BookAuthors.Add(new BookAuthor
            {
                AuthorId = authorEntry.Entity.Id,
                BookId = bookEntity.Entity.Id
            });
            context.SaveChanges();

            AuthorController controller = ControllerTestHelper.GetController<AuthorController, Author>(context, baseUri);

            //Act
            ActionResult result = await controller.DeleteBookAuthor(authorEntry.Entity.Id, bookEntity.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NoContent, (result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public async Task DeleteBookAuthor_ReturnsNotFound_IfEntitiesDoNotExist()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            AuthorController controller = ControllerTestHelper.GetController<AuthorController, Author>(context, baseUri);

            //Act
            ActionResult result = await controller.DeleteBookAuthor(Guid.NewGuid(), Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (result as StatusCodeResult).StatusCode);
        }
        #endregion
    }
}