using System;
using System.Net;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BookNote_Depository.App.AutoMapper;
using BookNote_Depository.App.Controllers;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Xunit;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.Tests.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace BookNote_Depository.Tests
{
    public class CrudBaseTests
    {
        string baseUri = "https://localhost:5001";

        /// <summary>
        /// Get an instance of CrudBase of a certain type of Entity and DTO with all dependecies taken care of
        /// </summary>
        private CrudBase<TEntity, TDTO> GetCrudBase<TEntity, TDTO>(ApplicationDbContext context, string baseUri)
            where TEntity : class
            where TDTO : class
        {
            IRepository<TEntity> repository = new Repository<TEntity>(context);
            MapperConfiguration mapperConfig = new MapperConfiguration(c => c.AddProfile<DomainProfile>());

            IPaginationUriService uriService = new PaginationUriService(baseUri);
            ILinkHeaderWriter<TEntity> headerWriter = new LinkHeaderWriter<TEntity>(uriService, context);

            ControllerContext controller = ControllerTestHelper.GetControllerContext();

            return new CrudBase<TEntity, TDTO>(repository, mapperConfig.CreateMapper(), headerWriter) 
                { ControllerContext = controller };
        }

        [Fact]
        public async Task Get_Returns_DTOs()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            context.Authors.Add(new Author());
            context.SaveChanges();

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult<List<AuthorDTO>> authors = await baseController.Get(new PaginationFilter(), new CancellationTokenSource().Token);

            //Assert
            Assert.Single(authors.Value);
        }

        [Fact]
        public async Task GetById_ReturnsEntity_IfItExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> entry = context.Authors.Add(new Author { Name = "John Doe" });
            context.SaveChanges();

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult<AuthorDTO> author = await baseController.GetById(entry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal(entry.Entity.Name, author.Value.Name);
            Assert.Equal(entry.Entity.Id, author.Value.Id);
        }

        [Fact]
        public async Task GetById_ReturnsNotFound_IfItDoesNotExists()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult<AuthorDTO> author = await baseController.GetById(Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (author.Result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public async Task Insert_()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            Author authorToCreate = new Author { Name = "John Doe" };

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult<AuthorDTO> author = await baseController.Insert(authorToCreate, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal(authorToCreate.Name, author.Value.Name);
        }

        [Fact]
        public async Task Update_()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            string name = "John Doe";
            EntityEntry<Author> entry = context.Authors.Add(new Author { Name = name });
            context.SaveChanges();

            Author auhtorToUpdate = entry.Entity;
            auhtorToUpdate.Name = "Jane Doe";

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult<AuthorDTO> author = await baseController.Update(auhtorToUpdate, new CancellationTokenSource().Token);

            //Assert
            Assert.NotEqual(name, author.Value.Name);
            Assert.Equal(auhtorToUpdate.Name, author.Value.Name);
        }

        [Fact]
        public async Task Delete_ReturnsNoContent_IfEntityDeleted()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            EntityEntry<Author> entry = context.Authors.Add(new Author());
            context.SaveChanges();

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult result = await baseController.Delete(entry.Entity.Id, new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NoContent, (result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public async Task Delete_ReturnsNotFound_IfEntityNotFound()
        {
            //Assign
            using ApplicationDbContext context = new ApplicationDbContext(
                DbContextTestHelper.GetOptions<ApplicationDbContext>());

            CrudBase<Author, AuthorDTO> baseController = GetCrudBase<Author, AuthorDTO>(context, baseUri);

            //Act
            ActionResult result = await baseController.Delete(Guid.NewGuid(), new CancellationTokenSource().Token);

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, (result as StatusCodeResult).StatusCode);
        }
    }
}