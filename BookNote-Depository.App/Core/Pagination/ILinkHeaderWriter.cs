using System;
using System.Linq;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.App.Core.Pagination
{
    public interface ILinkHeaderWriter<TEntity>
        where TEntity : class
    {
        void Write(IHeaderDictionary headers, PaginationFilter filter, string route);

        void Write<TQueryEntity>(IHeaderDictionary headers, PaginationFilter filter, string route, Func<DbSet<TEntity>, IQueryable<TQueryEntity>> query);
    }
}