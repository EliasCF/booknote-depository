using System;
using BookNote_Depository.App.Models.Pagination;

namespace BookNote_Depository.App.Core.Pagination
{
    public interface IPaginationUriService
    {
        Uri GetPageUri(PaginationFilter filter, string route);
    }
}