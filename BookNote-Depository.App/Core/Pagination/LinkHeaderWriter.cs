using System;
using System.Linq;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.App.Core.Pagination
{
    public class LinkHeaderWriter<TEntity> : ILinkHeaderWriter<TEntity>
        where TEntity : class
    {
        private readonly IPaginationUriService _uriService;

        private readonly ApplicationDbContext _context;

        public LinkHeaderWriter(
            IPaginationUriService uriService,
            ApplicationDbContext context)
        {
            _uriService = uriService;
            _context = context;
        }

        /// <summary>
        /// Write Link headers for a resources of TEntity.
        /// Obtain the total count of resources in the standart way.
        /// </summary>
        public void Write(IHeaderDictionary headers, PaginationFilter filter, string route)
        {
            int entityCount = _context.Set<TEntity>().Count();
            if (entityCount == 0) return;     

            _write(headers, filter, route, entityCount);
        }

        /// <summary>
        /// Write Link headers for a resources of TEntity.
        /// Obtain the total count of resources from a query provided as a parameter
        /// </summary>
        public void Write<TQueryEntity>(IHeaderDictionary headers, PaginationFilter filter, string route, Func<DbSet<TEntity>, IQueryable<TQueryEntity>> query)
        {
            int entityCount = query(_context.Set<TEntity>()).Count();
            if (entityCount == 0) return;  

            _write(headers, filter, route, entityCount);
        }

        private void _write(
            IHeaderDictionary headers,
            PaginationFilter filter,
            string route,
            int entityCount)
        {
            string linkHeader = string.Empty;

            int totalPages = Convert.ToInt32((double)entityCount / (double)filter.Size);
            if (filter.Page > totalPages) return;

            Uri firstPage = _uriService.GetPageUri(new PaginationFilter { Page = 1, Size = filter.Size }, route);
            linkHeader = string.Concat(linkHeader, _link(firstPage.ToString()), _rel("first"));

            if (filter.Page - 1 >= 1 && filter.Page <= totalPages)
            {
                Uri previousPage =_uriService.GetPageUri(new PaginationFilter { Page = filter.Page - 1, Size = filter.Size }, route);
                linkHeader = string.Concat(linkHeader, _link(previousPage.ToString()), _rel("prev"));
            }

            if (filter.Page >= 1 && filter.Page < totalPages)
            {
                Uri nextPage = _uriService.GetPageUri(new PaginationFilter { Page = filter.Page + 1, Size = filter.Size }, route);
                linkHeader = string.Concat(linkHeader, _link(nextPage.ToString()), _rel("next"));
            }

            Uri lastPage = _uriService.GetPageUri(new PaginationFilter { Page = totalPages, Size = filter.Size }, route);
            linkHeader = string.Concat(linkHeader, _link(lastPage.ToString()), _rel("last"));

            headers.Add("Link", linkHeader);
        }

        private string _link(string link) => string.Concat("<", link, ">");

        private string _rel(string name) => $"; rel=\"{name}\", ";
    }
}