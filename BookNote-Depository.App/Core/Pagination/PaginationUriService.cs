using System;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.AspNetCore.WebUtilities;

namespace BookNote_Depository.App.Core.Pagination
{
    public class PaginationUriService : IPaginationUriService
    {
        private readonly string _baseUri;

        public PaginationUriService(string baseUri)
        {
            _baseUri = baseUri;
        }

        public Uri GetPageUri(PaginationFilter filter, string route)
        {
            
            if (filter is null || string.IsNullOrEmpty(route) || string.IsNullOrEmpty(_baseUri)) return null;

            Uri enpointUri = new Uri(string.Concat(_baseUri, route));

            string modifiedUri = QueryHelpers.AddQueryString(enpointUri.ToString(), "Page", filter.Page.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "Size", filter.Size.ToString());

            return new Uri(modifiedUri);
        }
    }
}