using System.Threading;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookNote_Depository.App.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.App.Core.Extensions.Pagination;

namespace BookNote_Depository.App.Core.Extensions.Repository
{
    public static class UserRepositoryExtensions
    {
        /// <summary>
        /// Get all notes created by a user
        /// </summary>
        public static async Task<List<Note>> GetNotesByUserId(
            this IRepository<User> repository,
            Guid id,
            PaginationFilter filter,
            CancellationToken cancellationToken)
        {
            IQueryable<User> users = repository
                .AsQueryable()
                .Include(x => x.Books)
                    .ThenInclude(x => x.Notes)
                .Where(s => s.Id.Equals(id));
            if (!users.Any()) return null;

            IQueryable<Note> notes = users
                .SelectMany(x => x.Books)
                .SelectMany(x => x.Notes)
                .OrderBy(n => n.Id)
                .Paginate(filter);
            if (notes is null) return null;

            return await notes.ToListAsync();
        }

        /// <summary>
        /// Get all the books used by a user by user id
        /// </summary>
        public static async Task<List<Book>> GetBooksByUserId(
            this IRepository<User> repository,
            Guid id,
            PaginationFilter filter,
            CancellationToken cancellationToken)
        {
            IQueryable<User> users = repository
                .AsQueryable()
                .Include(q => q.Books)
                    .ThenInclude(q => q.Book)
                .Where(u => u.Id.Equals(id));
            if (!users.Any()) return null;

            IQueryable<Book> books = users
                .SelectMany(u => u.Books)
                .Select(b => b.Book)
                .OrderBy(b => b.Id)
                .Paginate(filter);
            if (books is null) return null;

            return await books.ToListAsync();
        }

        /// <summary>
        /// Created a relation between a user and a book
        /// </summary>
        public static async Task<bool> AddUserBook(
            this IRepository<User> repository,
            Guid userId,
            Guid bookId,
            CancellationToken cancellationToken)
        {
            User user = await repository.AsQueryable()
                .Include(u => u.Books)
                .SingleOrDefaultAsync(u => u.Id.Equals(userId), cancellationToken);
            if (user is null) return false;

            user.Books.Add(new UserBook
            {
                UserId = userId,
                BookId = bookId
            });

            await repository.UpdateAsync(user, cancellationToken);
            return true;
        }

        /// <summary>
        /// Delete a realtion between a user and a book
        /// </summary>
        public static async Task<bool> DeleteUserBook(
            this IRepository<User> repository,
            Guid userId,
            Guid bookId,
            CancellationToken cancellationToken)
        {
            User user = await repository.AsQueryable()
                .Include(u => u.Books)
                .SingleOrDefaultAsync(u => u.Id.Equals(userId), cancellationToken);
            if (user is null) return false;

            UserBook userBook = user.Books
                .SingleOrDefault(b => b.UserId.Equals(userId) && b.BookId.Equals(bookId));
            if (userBook is null) return false;

            user.Books.Remove(userBook);
            await repository.UpdateAsync(user);

            return true;
        }
    }
}