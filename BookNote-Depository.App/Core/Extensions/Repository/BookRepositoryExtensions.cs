using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Core.Extensions.Pagination;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.App.Core.Extensions.Repository
{
    public static class BookRepositoryExtensions
    {
        /// <summary>
        /// Get all authors of a book by the id of the book
        /// </summary>
        public async static Task<List<Author>> GetAuthorsByBookId(
            this IRepository<Book> repository,
            Guid id,
            PaginationFilter filter,
            CancellationToken cancellationToken)
        {
            IQueryable<Book> books = repository
                .AsQueryable()
                .Include(q => q.BookAuthors)
                    .ThenInclude(q => q.Author)
                .Where(b => b.Id.Equals(id));
            if (!books.Any()) return null;

            IQueryable<Author> authors = books
                .SelectMany(b => b.BookAuthors)
                .Select(u => u.Author)
                .OrderBy(u => u.Id)
                .Paginate(filter);
            if (authors is null) return null;
            
            return await authors.ToListAsync();
        }

        /// <summary>
        /// Get all users using the book by the id of the book
        /// </summary>
        public async static Task<List<User>> GetUsersByBookId(
            this IRepository<Book> repository,
            Guid id,
            PaginationFilter filter,
            CancellationToken cancellationToken)
        {
            IQueryable<Book> books =  repository
                .AsQueryable()
                .Include(q => q.Users)
                    .ThenInclude(q => q.User)
                .Where(b => b.Id.Equals(id));
            if (!books.Any()) return null;

            IQueryable<User> users = books
                .SelectMany(b => b.Users)
                .Select(u => u.User)
                .OrderBy(u => u.Id)
                .Paginate(filter);
            if (users is null) return null;

            return await users.ToListAsync();
        }
    }
}