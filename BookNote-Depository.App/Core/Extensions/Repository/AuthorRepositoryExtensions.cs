using System.Threading;
using System.Collections.Generic;
using BookNote_Depository.App.Models.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.App.Core.Extensions.Pagination;

namespace BookNote_Depository.App.Core.Extensions.Repository
{
    public static class AuthorRepositoryExtensions
    {
        /// <summary>
        /// Get all books by the id of the author
        /// </summary>
        public static async Task<List<Book>> GetBooksByAuthorId(
            this IRepository<Author> repository,
            Guid id,
            PaginationFilter filter,
            CancellationToken cancellationToken)
        {
            IQueryable<Author> authors = repository
                .AsQueryable()
                .Include(x => x.BookAuthors)
                    .ThenInclude(x => x.Book)
                .Where(s => s.Id.Equals(id));
            if (!authors.Any()) return null;

            IQueryable<Book> books = authors
                .SelectMany(x => x.BookAuthors)
                .Select(x => x.Book)
                .OrderBy(b => b.Id)
                .Paginate(filter);
            if (books is null) return null;

            return await books.ToListAsync();
        }

        /// <summary>
        /// Add a new relation between a book and an author
        /// </summary>
        public static async Task<bool> AddBookAuthor(
            this IRepository<Author> repository,
            Guid authorId,
            Guid bookId,
            CancellationToken cancellationToken)
        {
            Author author = await repository.AsQueryable()
                .Include(a => a.BookAuthors)
                .FirstOrDefaultAsync(a => a.Id.Equals(authorId), cancellationToken);
            if (author is null) return false;

            author.BookAuthors.Add(new BookAuthor
            {
                AuthorId = authorId,
                BookId = bookId
            });

            await repository.UpdateAsync(author, cancellationToken);
            return true;
        }

        /// <summary>
        /// Delete a relation between a book and an author
        /// </summary>
        public static async Task<bool> DeleteBookAuthor(
            this IRepository<Author> repository,
            Guid authorId,
            Guid bookId,
            CancellationToken cancellationToken)
        {
            Author author = await repository.AsQueryable()
                .Include(a => a.BookAuthors)
                .SingleOrDefaultAsync(a => a.Id.Equals(authorId), cancellationToken);
            if (author is null) return false;

            BookAuthor bookAuthor = author.BookAuthors
                .FirstOrDefault(b => b.AuthorId.Equals(authorId) && b.BookId.Equals(bookId));
            if (bookAuthor is null) return false;

            author.BookAuthors.Remove(bookAuthor);
            await repository.UpdateAsync(author, cancellationToken);

            return true;
        }
    }
}