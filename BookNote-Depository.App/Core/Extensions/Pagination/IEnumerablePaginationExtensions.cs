using System.Collections.Generic;
using System.Linq;
using BookNote_Depository.App.Models.Pagination;

namespace BookNote_Depository.App.Core.Extensions.Pagination
{
    public static class IEnumerablePaginationExtensions
    {
        public static IEnumerable<TEntity> Paginate<TEntity>(this IEnumerable<TEntity> entities, PaginationFilter filter)
            where TEntity : class
        {
            if (filter.Page <= 0 || filter.Size <= 0) return null;

            return entities
                .Skip((filter.Page - 1) * filter.Size)
                .Take(filter.Size);
        }

        public static IQueryable<TEntity> Paginate<TEntity>(this IQueryable<TEntity> entities, PaginationFilter filter)
            where TEntity : class
        {
                if (filter.Page <= 0 || filter.Size <= 0) return null;

                return entities
                    .Skip((filter.Page - 1) * filter.Size)
                    .Take(filter.Size);
        }
    }
}