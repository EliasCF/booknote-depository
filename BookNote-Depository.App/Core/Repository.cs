using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BookNote_Depository.App.Core.Extensions.Pagination;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace BookNote_Depository.App.Core
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly ApplicationDbContext _context;

        private readonly DbSet<TEntity> _entities;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        /// <summary>
        /// Get all entities as queryable
        /// </summary>
        public IQueryable<TEntity> AsQueryable()
        {
            return _entities.AsQueryable();
        }

        /// <summary>
        /// Delete an entity by id
        /// </summary>
        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            TEntity entity = await _entities.FindAsync(new object[] { id }, cancellationToken);
            if (entity is null) return false;

            _entities.Remove(entity);
            await _context.SaveChangesAsync(cancellationToken);

            return true;
        }

        /// <summary>
        /// Get all entities
        /// </summary>
        public async Task<List<TEntity>> GetAsync(PaginationFilter filter, CancellationToken cancellationToken = default)
        {
            IQueryable<TEntity> paginatedEntities = _entities.Paginate(filter);
            if (paginatedEntities is null) return null;

            return await paginatedEntities.ToListAsync(cancellationToken);
        }

        /// <summary>
        /// Get an entity by id
        /// </summary>
        public async Task<TEntity> GetAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await _entities.FindAsync(new object[] { id }, cancellationToken);
        }

        /// <summary>
        /// Insert a new entity
        /// </summary>
        public async Task<TEntity> InsertAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            EntityEntry<TEntity> entry = await _entities.AddAsync(entity, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);

            return entry.Entity;
        }

        /// <summary>
        /// Update the data of an entity
        /// </summary>
        public async Task<TEntity> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            EntityEntry<TEntity> entry = _entities.Update(entity);
            await _context.SaveChangesAsync(cancellationToken);

            return entry.Entity;
        }
    }
}