using System.Linq;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookNote_Depository.App.Models.Pagination;

namespace BookNote_Depository.App.Core
{
    public interface IRepository<TEntity>
    {
        IQueryable<TEntity> AsQueryable();

        Task<List<TEntity>> GetAsync(PaginationFilter pagionation, CancellationToken cancellationToken = default);

        Task<TEntity> GetAsync(Guid id, CancellationToken cancellationToken = default);

        Task<TEntity> InsertAsync(TEntity entity, CancellationToken cancellationToken = default);

        Task<TEntity> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken = default);
    }
}