using System.Threading;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookNote_Depository.App.Core;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.App.Core.Pagination;

namespace BookNote_Depository.App.Controllers
{
    public class CrudBase<TEntity, TDTO> : Controller
        where TEntity : class
        where TDTO : class
    {
        private readonly IRepository<TEntity> _repository;

        private readonly IMapper _mapper;

        private readonly ILinkHeaderWriter<TEntity> _headerWriter;

        public CrudBase(
            IRepository<TEntity> repository,
            IMapper mapper,
            ILinkHeaderWriter<TEntity> headerWriter)
        {
            _repository = repository;
            _mapper = mapper;
            _headerWriter = headerWriter;
        }

        [HttpGet]
        public virtual async Task<ActionResult<List<TDTO>>> Get([FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
        {
            List<TEntity> entity = await _repository.GetAsync(filter, cancellationToken);

            _headerWriter.Write(Response.Headers, filter, Request.Path.Value);

            List<TDTO> dataTransferObjects = _mapper.Map<List<TDTO>>(entity);
            return dataTransferObjects;
        }

        [HttpGet("{id}")]
        public virtual async Task<ActionResult<TDTO>> GetById(Guid id, CancellationToken cancellationToken)
        {
            TEntity entity = await _repository.GetAsync(id, cancellationToken);
            if (entity is null) return NotFound();

            TDTO dataTransferObject = _mapper.Map<TDTO>(entity);
            return dataTransferObject;
        }

        [HttpPost]
        public virtual async Task<ActionResult<TDTO>> Insert([FromBody] TEntity entity, CancellationToken cancellationToken)
        {
            TEntity createdEntity = await _repository.InsertAsync(entity, cancellationToken);

            TDTO dataTransferObject = _mapper.Map<TDTO>(createdEntity);
            return dataTransferObject;
        }

        [HttpPut]
        public virtual async Task<ActionResult<TDTO>> Update([FromBody] TEntity entity, CancellationToken cancellationToken)
        {
            TEntity updatedEntity = await _repository.UpdateAsync(entity, cancellationToken);

            TDTO dataTransferObject = _mapper.Map<TDTO>(updatedEntity);
            return dataTransferObject;
        }

        [HttpDelete]
        public virtual async Task<ActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            bool result = await _repository.DeleteAsync(id, cancellationToken);
            if (!result) return NotFound();

            return NoContent();
        }
    }
}