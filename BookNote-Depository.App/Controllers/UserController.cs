using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Extensions.Repository;
using BookNote_Depository.App.Models.Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using BookNote_Depository.App.Models.DTO;
using AutoMapper;
using BookNote_Depository.App.Models.Pagination;
using BookNote_Depository.App.Core.Pagination;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BookNote_Depository.App.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : CrudBase<User, UserDTO>
    {
        private readonly IRepository<User> _repository;

        private readonly IMapper _mapper;

        private readonly ILinkHeaderWriter<User> _headerWriter;

        public UserController(
            IRepository<User> repository,
            IMapper mapper,
            ILinkHeaderWriter<User> headerWriter)
                : base(repository, mapper, headerWriter)
        {
            _repository = repository;
            _mapper = mapper;
            _headerWriter = headerWriter;
        }

        [HttpGet("{userId}/notes")]
        public async Task<ActionResult<List<NoteDTO>>> GetNotesByUserId(Guid userId, [FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
        {
            List<Note> notes = await _repository.GetNotesByUserId(userId, filter, cancellationToken);
            if (notes is null) return NotFound();

            _headerWriter.Write<Note>(Response.Headers, filter, Request.Path.Value, x => x
                .Include(x => x.Books)
                .ThenInclude(x => x.Notes)
                .Where(s => s.Id.Equals(userId))
                .SelectMany(x => x.Books)
                .SelectMany(x => x.Notes));

            List<NoteDTO> dataTransferObjects = _mapper.Map<List<NoteDTO>>(notes);
            return dataTransferObjects;
        }

        [HttpGet("{userId}/books")]
        public async Task<ActionResult<List<BookDTO>>> GetBooksByUserId(Guid userId, [FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
        {
            List<Book> books = await _repository.GetBooksByUserId(userId, filter, cancellationToken);
            if (books is null) return NotFound();

            _headerWriter.Write<Book>(Response.Headers, filter, Request.Path.Value, query => query
                .Include(q => q.Books)
                .ThenInclude(q => q.Book)
                .Where(u => u.Id.Equals(userId))
                .SelectMany(u => u.Books)
                .Select(b => b.Book));

            List<BookDTO> dataTransferObjects = _mapper.Map<List<BookDTO>>(books);
            return dataTransferObjects;
        }

        [HttpPut("{userId}/books/{bookId}")]
        public async Task<ActionResult> AddUserBook(Guid userId, Guid bookId, CancellationToken cancellationToken)
        {
            bool result = await _repository.AddUserBook(userId, bookId, cancellationToken);
            if (!result) return NotFound();

            return Ok();
        }

        [HttpDelete("{userId}/books/{bookId}")]
        public async Task<ActionResult> DeleteUserBook(Guid userId, Guid bookId, CancellationToken cancellationToken)
        {
            bool result = await _repository.DeleteUserBook(userId, bookId, cancellationToken);
            if (!result) return NotFound();

            return NoContent();
        }
    }
}