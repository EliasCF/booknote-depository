using AutoMapper;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using Microsoft.AspNetCore.Mvc;

namespace BookNote_Depository.App.Controllers
{
    [ApiController]
    [Route("api/notes")]
    public class NoteController : CrudBase<Note, NoteDTO>
    {
        public NoteController(
            IRepository<Note> repository,
            IMapper mapper,
            ILinkHeaderWriter<Note> headerWriter)
                : base(repository, mapper, headerWriter) { }
    }
}