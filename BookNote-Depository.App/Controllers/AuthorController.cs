using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Extensions.Repository;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.App.Controllers
{
    [ApiController]
    [Route("api/authors")]
    public class AuthorController : CrudBase<Author, AuthorDTO>
    {
        private readonly IRepository<Author> _repository;

        private readonly IMapper _mapper;

        private readonly ILinkHeaderWriter<Author> _headerWriter;

        public AuthorController(
            IRepository<Author> repository,
            IMapper mapper,
            ILinkHeaderWriter<Author> headerWriter)
                : base(repository, mapper, headerWriter)
        {
            _repository = repository;
            _mapper = mapper;
            _headerWriter = headerWriter;
        }

        [HttpGet("{authorId}/books")]
        public async Task<ActionResult<List<BookDTO>>> GetBooksByAuthor(Guid authorId, [FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
        {
            List<Book> books = await _repository.GetBooksByAuthorId(authorId, filter, cancellationToken);
            if (books is null) return NotFound();

            _headerWriter.Write<Book>(Response.Headers, filter, Request.Path.Value, query => query
                .Include(q => q.BookAuthors)
                    .ThenInclude(q => q.Book)
                .Where(a => a.Id.Equals(authorId))
                .SelectMany(a => a.BookAuthors)
                .Select(b => b.Book));

            List<BookDTO> dataTransferObjects = _mapper.Map<List<BookDTO>>(books);
            return dataTransferObjects;
        }

        [HttpPut("{authorId}/books/{bookId}")]
        public async Task<ActionResult> AddBookAuthor(Guid authorId, Guid bookId, CancellationToken cancellationToken)
        {
            bool result = await _repository.AddBookAuthor(authorId, bookId, cancellationToken);
            if (!result) return NotFound();

            return Ok();
        }

        [HttpDelete("{authorId}/books/{bookId}")]
        public async Task<ActionResult> DeleteBookAuthor(Guid authorId, Guid bookId, CancellationToken cancellationToken)
        {
            bool result = await _repository.DeleteBookAuthor(authorId, bookId, cancellationToken);
            if (!result) return NotFound();

            return NoContent();
        }
    }
}