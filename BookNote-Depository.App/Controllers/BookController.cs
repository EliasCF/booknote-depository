using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Extensions.Repository;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;
using BookNote_Depository.App.Models.Pagination;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.App.Controllers
{
    [ApiController]
    [Route("api/books")]
    public class BookController : CrudBase<Book, BookDTO>
    {
        private readonly IRepository<Book> _repository;

        private readonly IMapper _mapper;

        private readonly ILinkHeaderWriter<Book> _headerWriter;

        public BookController(
            IRepository<Book> repository,
            IMapper mapper,
            ILinkHeaderWriter<Book> headerWriter)
                : base(repository, mapper, headerWriter)
        {
            _repository = repository;
            _mapper = mapper;
            _headerWriter = headerWriter;
        }

        [HttpGet("{bookId}/users")]
        public async Task<ActionResult<List<UserDTO>>> GetUsersByBookId(Guid bookId, [FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
        {
            List<User> users = await _repository.GetUsersByBookId(bookId, filter, cancellationToken);
            if (users is null) return NotFound();

            _headerWriter.Write<User>(Response.Headers, filter, Request.Path.Value, query => query
                .Include(q => q.Users)
                    .ThenInclude(q => q.User)
                .Where(b => b.Id.Equals(bookId))
                .SelectMany(b => b.Users)
                .Select(u => u.User));

            List<UserDTO> dataTransferObjects = _mapper.Map<List<UserDTO>>(users);
            return dataTransferObjects;
        }

        [HttpGet("{bookId}/authors")]
        public async Task<ActionResult<List<AuthorDTO>>> GetAuthorByBookId(Guid bookId, [FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
        {
            List<Author> authors = await _repository.GetAuthorsByBookId(bookId, filter, cancellationToken);
            if (authors is null) return NotFound();

            _headerWriter.Write<Author>(Response.Headers, filter, Request.Path.Value, query => query
                .Include(q => q.BookAuthors)
                    .ThenInclude(q => q.Author)
                .Where(b => b.Id.Equals(bookId))
                .SelectMany(b => b.BookAuthors)
                .Select(u => u.Author));

            List<AuthorDTO> dataTransferObjects = _mapper.Map<List<AuthorDTO>>(authors);
            return dataTransferObjects;
        }
    }
}