using BookNote_Depository.App.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace BookNote_Depository.App.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<UserBook> UserBooks { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            /*
             *  Note
             */
            builder.Entity<Note>()
                .HasOne(n => n.UserBook)
                .WithMany(u => u.Notes)
                .HasForeignKey(n => new { n.UserBookUserId, n.UserBookBookId });

            /*
             *  UserBook
             */
            builder.Entity<UserBook>()
                .HasKey(u => new { u.UserId, u.BookId });

            /* 
             *  BookAuthor
             */
            builder.Entity<BookAuthor>()
                .HasKey(b => new { b.BookId, b.AuthorId });
        }
    }
}