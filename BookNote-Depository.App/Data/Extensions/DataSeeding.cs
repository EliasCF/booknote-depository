using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Bogus;
using BookNote_Depository.App.Models.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace BookNote_Depository.App.Data.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class DataSeeding
    {
        public static void GenerateData(this IApplicationBuilder builder)
        {
            using IServiceScope scope = builder.ApplicationServices.CreateScope();
            using ApplicationDbContext context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

            if (context.Books.Any()) return;

            List<User> users = new Faker<User>()
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.UserName, f => f.Person.UserName)
                .Generate(10);
            context.Users.AddRange(users);

            List<Book> books = new Faker<Book>()
                .RuleFor(b => b.Title, f => f.Person.LastName)
                .RuleFor(b => b.ISBN, f => f.Random.String())
                .RuleFor(b => b.CoverURL, f => f.Internet.Url())
                .Generate(10);
            context.Books.AddRange(books);

            books.ForEach(book =>
            {
                Author author = new Faker<Author>()
                    .RuleFor(a => a.Name, f => f.Name.FullName())
                    .Generate();
                context.Authors.Add(author);

                BookAuthor bookAuthor = new BookAuthor
                {
                    AuthorId = author.Id,
                    BookId = book.Id
                };
                context.BookAuthors.Add(bookAuthor);
            });

            users.ForEach(user =>
            {
                books.ForEach(book =>
                {
                    UserBook userBook = new UserBook
                    {
                        UserId = user.Id,
                        BookId = book.Id
                    };
                    context.UserBooks.Add(userBook);

                    List<Note> notes = new Faker<Note>()
                        .RuleFor(n => n.Title, f => f.Lorem.Slug(20))
                        .RuleFor(n => n.Content, f => f.Lorem.Paragraph())
                        .RuleFor(n => n.CreatedDate, f => DateTime.Now.AddDays(-2))
                        .RuleFor(n => n.UpdatedDate, f => DateTime.Now.AddMinutes(-5))
                        .RuleFor(n => n.UserBookUserId, f => user.Id)
                        .RuleFor(n => n.UserBookBookId, f => book.Id)
                        .Generate(2);
                    context.Notes.AddRange(notes);
                });
            });

            context.SaveChanges();
        }
    }
}