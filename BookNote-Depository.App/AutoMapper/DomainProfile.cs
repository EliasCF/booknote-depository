using AutoMapper;
using BookNote_Depository.App.Models.Domain;
using BookNote_Depository.App.Models.DTO;

namespace BookNote_Depository.App.AutoMapper
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Author, AuthorDTO>();
            CreateMap<Book, BookDTO>();
            CreateMap<Note, NoteDTO>();
            CreateMap<User, UserDTO>();
        }
    }
}