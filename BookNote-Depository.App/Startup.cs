using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using BookNote_Depository.App.AutoMapper;
using BookNote_Depository.App.Core;
using BookNote_Depository.App.Core.Pagination;
using BookNote_Depository.App.Data;
using BookNote_Depository.App.Data.Extensions;
using BookNote_Depository.App.Models.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BookNote_Depository.App
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetSection("Database:ConnectionString").Value;
            services.AddDbContext<ApplicationDbContext>(options
                => options.UseSqlServer(connectionString));

            services.AddControllersWithViews();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddAutoMapper(typeof(DomainProfile));

            services.AddScoped<IRepository<Author>, Repository<Author>>();
            services.AddScoped<IRepository<Book>, Repository<Book>>();
            services.AddScoped<IRepository<Note>, Repository<Note>>();
            services.AddScoped<IRepository<User>, Repository<User>>();

            services.AddHttpContextAccessor();
            services.AddSingleton<IPaginationUriService>(options =>
            {
                IHttpContextAccessor accessor = options.GetRequiredService<IHttpContextAccessor>();
                HttpRequest request = accessor.HttpContext.Request;

                string uri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                return new PaginationUriService(uri);
            });

            services.AddScoped(typeof(ILinkHeaderWriter<>), typeof(LinkHeaderWriter<>));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.GenerateData();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
