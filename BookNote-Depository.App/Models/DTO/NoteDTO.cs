using System;

namespace BookNote_Depository.App.Models.DTO
{
    public class NoteDTO
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public Guid UserBookUserId { get; set; }

        public Guid UserBookBookId { get; set; }
    }
}