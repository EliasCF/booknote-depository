using System;

namespace BookNote_Depository.App.Models.DTO
{
    public class BookDTO
    {
        public Guid Id { get; set; }

        public string ISBN { get; set; }

        public string Title { get; set; }

        public string CoverURL { get; set; }
    }
}