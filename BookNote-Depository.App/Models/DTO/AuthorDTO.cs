using System;

namespace BookNote_Depository.App.Models.DTO
{
    public class AuthorDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}