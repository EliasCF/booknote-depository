namespace BookNote_Depository.App.Models.Pagination
{
    public class PaginationFilter
    {
        /// <summary>
        /// The number of the current page
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// Amount of items per page
        /// </summary>
        public int Size { get; set; } = 10;
    }
}