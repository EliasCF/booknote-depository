using System;
using System.Collections.Generic;

namespace BookNote_Depository.App.Models.Domain
{
    public class UserBook
    {
        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid BookId { get; set; }
        public Book Book { get; set; }

        public virtual ICollection<Note> Notes { get; set; }
    }
}