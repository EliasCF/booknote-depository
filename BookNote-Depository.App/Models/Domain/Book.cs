using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookNote_Depository.App.Models.Domain
{
    public class Book
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string ISBN { get; set; }

        [Required]
        public string Title { get; set; }

        public string CoverURL { get; set; }

        public virtual ICollection<UserBook> Users { get; set; }

        public virtual ICollection<BookAuthor> BookAuthors { get; set; }
    }
}